<?php

namespace App\Entity;

readonly class Organization
{
    public function __construct(
        public string $name,
        public string $visibility,
    ) {
    }
}
