<?php

namespace App\Entity;

class Site
{
    private string $realUrl = '';

    /** @var string[] */
    private array $headers = [];

    public function __construct(
        public readonly string $url,
        public string $spip = '',
        public string $php = '',
        public string $ip = '',
        public string $server = '',
        /** @var Plugin[] */
        public array $plugins = [],
    ) {
    }

    public function setPluginsFromString(string $plugins): self
    {
        $list = [];
        foreach (explode(',', \trim(\str_replace(' ', '', $plugins))) as $plugin) {
            $list[] = Plugin::fromString($plugin);
        }
        $this->plugins = $list;

        return $this;
    }

    public function getPluginsAsString(): string
    {
        return implode(',', array_map(function (Plugin $plugin) {
            return $plugin->getFullName();
        }, $this->plugins));
    }

    public function setRealUrl(string $url): self
    {
        $this->realUrl = $url;

        return $this;
    }

    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    public function getHeader(string $header): string
    {
        if (isset($this->headers[$header])) {
            return $this->headers[$header][0];
        }

        return '';
    }

    public function getRealUrl(): string
    {
        return $this->realUrl;
    }
}
