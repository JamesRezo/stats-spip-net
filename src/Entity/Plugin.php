<?php

namespace App\Entity;

use Exception;

class Plugin
{
    private function __construct(
        public string $name,
        public string $version,
    ) {
    }

    public function getFullName(): string
    {
        return $this->name . '(' . $this->version . ')';
    }

    public static function fromString(string $fullName): self
    {
        // plugin(x.y.z) OK
        // plugin(x.y.z (spip192)) OK
        if (\preg_match(',^([a-z0-9_]+)\(([\d.]+).*\)$,', $fullName, $matches)) {
            return new self($matches[1], $matches[2]);
        }

        // @todo Si une chaine désignant un plugin est mal formée, ne pas bloquer
        throw new Exception('wrong string');
    }
}
