<?php

namespace App\Entity;

use DateTimeImmutable;

readonly class Repository
{
    public function __construct(
        public string $name,
        public string $fullName,
        public bool $empty,
        public bool $private,
        public string $defaultBranch,
        public bool $archived,
        public DateTimeImmutable $createdAt,
        public DateTimeImmutable $updatedAt,
    ) {
    }
}
