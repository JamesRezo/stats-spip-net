<?php

namespace App\Command;

use App\Entity\Repository;
use App\Service\GiteaPaginator;
use DateTime;
use DateTimeImmutable;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:spipnet:repos',
    description: 'Polls git.spip.net repositories.'
)]
class SpipNetRepoCommand extends Command
{
    public function __construct(
        private GiteaPaginator $paginator,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'Limit for pagination', 50);
        $this->addOption('organization', 'o', InputOption::VALUE_REQUIRED, 'Restrict to one organization.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = $input->getOption('limit');
        $organization = $input->getOption('organization');
        if (is_null($organization)) {
            $output->writeln('fetching all organizations...');
            // @todo get organizations from database
            $organizations = [];
            $output->writeln('<error>Not implemented yet.</>');
            return Command::FAILURE;
        } else {
            $organizations = [$organization];
        }

        //foreach $organizations, refresh repos
        $responses = [];
        foreach ($organizations as $organization) {
            $maxPage = $this->paginator->withLimit($limit)->findMaxPages('/api/v1/orgs/{organization}/repos', [
                'organization' => $organization,
            ]);
            for ($page=0; $page <= $maxPage; $page++) { 
                $responses[] = $this->paginator->getPage('/api/v1/orgs/{organization}/repos', $page + 1, [
                    'organization' => $organization,
                ]);
            }    
        }
        $pages = $this->paginator->getAll($responses);
        $repositories = \array_map(function($repository) {
            return new Repository(
                $repository['name'],
                $repository['full_name'],
                $repository['empty'],
                $repository['private'],
                $repository['default_branch'],
                $repository['archived'],
                DateTimeImmutable::createFromFormat('Y-m-d\TH:i:sT', $repository['created_at']),
                DateTimeImmutable::createFromFormat('Y-m-d\TH:i:sT', $repository['updated_at']),
            );
        }, $pages);


        //foreach repo, refresh branches&tags as refs, hooks
        //foreach ref, find paquet.xml, find plugin.xml, find composer.json
        //foreach paquet.xml, find prefix, version, compatibilite spip, necessite (plugin/contrainte[])
        //foreach plugin.xml, find prefix, version, compatibilite spip, necessite (plugin/contrainte[])
        //foreach composer.json, find name, type, require spip/framework

        // @todo store organizations in database
        $output->writeln(array_map(function (Repository $repository) {
            return $repository->fullName . ($repository->archived ? ' (archivé)' : '');
        }, $repositories));

        return Command::SUCCESS;
    }
}
