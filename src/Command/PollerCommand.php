<?php

namespace App\Command;

use App\Entity\Site;
use App\Service\Detector;
use App\Service\UrlParser;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(
    name: 'app:poll',
    description: 'Polls a website to find its SPIP version and more.'
)]
class PollerCommand extends Command
{
    public function __construct(
        private HttpClientInterface $client,
        private Detector $detector,
        private UrlParser $parser,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('url', InputArgument::REQUIRED, 'URL to poll');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $site = new Site($this->parser->parse($input->getArgument('url'))->glue());
            $response = $this->client->request('HEAD', $site->url);
            $site
                ->setHeaders($response->getHeaders())
                ->setRealUrl($response->getInfo('url'))
            ;
            // Alternative à `nslookup <host> | grep -e "^Address: " | cut -d' ' -f2`
            $site->ip = filter_var(
                gethostbyname($this->parser->parse($site->getRealUrl())->getHost()),
                FILTER_VALIDATE_IP
            ) ?: '';
        } catch (TransportException $e) {
            // ex: touchalon.free.fr -> touchalon.com -> No DNS
            if (\str_starts_with($e->getMessage(), 'Could not resolve host:')) {
                $output->writeln('<error>No DNS</>');
                $output->writeln('URL   : ' . $response->getInfo('url'));
                $output->writeln('Status: no-dns');
                return Command::FAILURE;
            }
            // ex: http://www.premiumorange.com/ceeja/spip/
            if (\str_starts_with($e->getMessage(), 'Idle timeout reached for')) {
                $output->writeln('<error>Timeout</>');
                $output->writeln('URL   : ' . $response->getInfo('url'));
                $output->writeln('Retry : +1');
                // @todo Si retry>X Alors status=dead/timeout,statut=poub?
                return Command::FAILURE;
            }
            // ex: http://localhost:8080
            if (\str_starts_with($e->getMessage(), 'Failed to connect to'))  {
                $output->writeln('<error>Not a web server</>');
                $output->writeln('URL   : ' . $response->getInfo('url'));
                $output->writeln('Status: fatal');
                return Command::FAILURE;
            }
        } catch (RedirectionException $e) {
            $output->writeln('<error>Too many redirection</>');
            $output->writeln('URL   : ' . $response->getInfo('url'));
            $output->writeln('Retry : +1');
            // @todo Si retry>X Alors status=dead/timeout,statut=poub?
            return Command::FAILURE;
        } catch (ClientException $e) {
            // ex: http://sanchez90.free.fr/spip/, http://sanchez90.free.fr/
            if (in_array($e->getCode(), [403, 404])) {
                // @todo url propres ou sous-sites: si path a 2 `/` ou plus, il faut boucler au cas où 
                $output->writeln('<error>Error 40x</>');
                $output->writeln('URL   : ' . $response->getInfo('url'));
                $output->writeln('Status: dead');
                // @todo Certains sites renvoie une 403 au lieu d'une 405 quand il refuse les HEAD
                // @todo Faire un GET, mais moins souvent
            } elseif ($e->getCode() == 405) {
                $output->writeln('<error>Error 40x</>');
                $output->writeln('URL   : ' . $response->getInfo('url'));
                // @todo Faire un GET, mais moins souvent
            } else {
                $output->writeln('code  : ' . $e->getCode());
                $output->writeln('erreur: ' . $e->getMessage());
            }
            return Command::FAILURE;
        } catch (\Throwable $th) {
            // @todo catch(ServerExceptionInterface $e) si error 50x
            // ssl error "SSL: no alternative certificate subject name matches target host name"
            // ...
            $output->writeln('code  : ' . $th->getCode());
            $output->writeln('erreur: ' . $th->getMessage());
            $output->writeln('class : ' . \get_class($th));

            return Command::FAILURE;
        }

        $this->detector->setSite($site);
        $composedBy = $this->detector->checkComposedBy();
        if ($composedBy) {
            $output->writeln('Composed by : ' . $composedBy, OutputInterface::VERBOSITY_DEBUG);

            $spip = \preg_replace(',^SPIP ([\w.]+) @.*,i', '$1', $composedBy);
            // Composed-By: SPIP @ www.spip.net
            $header_silencieux = $spip == $composedBy;
            $site->spip = $header_silencieux ? '' : $spip;
            if(\preg_match(',config\.txt$,i', $composedBy)) {
                $localConfig = \preg_replace(',.* (https?://.*/config\.txt)$,', '$1', $composedBy);
                $output->writeln('les plugins sont à récupérér dans le fichier "' . $localConfig . '"', OutputInterface::VERBOSITY_DEBUG);
                $localConfigResponse = $this->client->request('GET', $localConfig);
                $site->setPluginsFromString(\preg_replace('/.* \+ ([\w.\(\), ]+)$/', '$1', $localConfigResponse->getContent()));
                // @todo si erreur 40x -> pas de plugin détectable mais ne pas bloquer
            } else {
                if (!$header_silencieux) {
                    $output->writeln('les plugins sont à récupérér dans composed-by', OutputInterface::VERBOSITY_DEBUG);
                    $site->setPluginsFromString(\preg_replace('/.* \+ ([\w.\(\), ]+)$/', '$1', $composedBy));
                }
            }
        } else {
            $output->writeln('Header Composed-By: manquant');
            // @todo chercher la version de spip ailleurs sans bloquer sur 40x
            // tenter GET ${realUrl}/local/config.txt
            // tenter GET ${realUrl}/ecrire/paquet.xml
            // Tenter GET ${realUrl}/spip.php?page=login scrap <meta name="generator" content="SPIP ([^"]+)" />
            // Tenter GET ${realUrl}/htaccess.txt scrap "^# Fichier .htaccess\s+SPIP v ([\d.]+) #$"
            // Si KO : poub
            $output->writeln('Not a SPIP site or too old version (<1.8 ?)');
        }

        $site->php = $this->detector->checkPhp();
        $site->server = $this->detector->checkServer();

        try {
            $output->writeln('Status : ' . $response->getStatusCode(), OutputInterface::VERBOSITY_DEBUG);
            $output->writeln([
                'URL    : ' . $site->getRealUrl(),
                'IP     : ' . $site->ip,
                'SPIP   : ' . $site->spip,
                'PHP    : ' . $site->php,
                'Server : ' . $site->server,
                'Plugins: ' . $site->getPluginsAsString(),
            ]);
        } catch (TransportException $e) {
            // ex: http://nav.sfr.fr/default.php
            if (\str_starts_with($e->getMessage(), 'Recv failure: Connection reset by peer')) {
                $output->writeln('<error>RST packets failure</>');
                $output->writeln('Status: fatal');
                return Command::FAILURE;
            }
        } catch (\Throwable $th) {
            $output->writeln('code  : ' . $th->getCode());
            $output->writeln('erreur: ' . $th->getMessage());
            $output->writeln('class : ' . \get_class($th));
        }

        return Command::SUCCESS;
    }
}
