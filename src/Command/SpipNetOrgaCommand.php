<?php

namespace App\Command;

use App\Entity\Organization;
use App\Service\GiteaPaginator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:spipnet:orgas',
    description: 'Polls git.spip.net organizaztions.'
)]
class SpipNetOrgaCommand extends Command
{
    public function __construct(
        private GiteaPaginator $paginator,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'Limit for pagination', 50);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = $input->getOption('limit');
        $maxPage = $this->paginator->withLimit($limit)->findMaxPages('/api/v1/admin/orgs');

        $responses = [];
        for ($page=0; $page <= $maxPage; $page++) { 
            $responses[] = $this->paginator->getPage('/api/v1/admin/orgs', $page + 1);
        }
        $pages = $this->paginator->getAll($responses);
        $organizations = \array_map(function($organization) {
            return new Organization($organization['name'], $organization['visibility']);
        }, $pages);

        // @todo store organizations in database
        $output->writeln(array_map(function (Organization $organization) {
            return $organization->name;
        }, $organizations));

        return Command::SUCCESS;
    }
}
