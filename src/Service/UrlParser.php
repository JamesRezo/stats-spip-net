<?php

namespace App\Service;

use Exception;

class UrlParser
{
    private array $parsed = [];

    public function parse(string $url): self {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception('wrong url');
        }

        $parsed = \parse_url($url);
        if (empty($parsed)) {
            // ex: url in ('', 'http://', ...)
            throw new Exception('url empty or incomplete');
        }
        if (!\preg_match(',https?,', $parsed['scheme'])) {
            // ex: url in ('mysql://', ...)
            throw new Exception('wrong scheme url');
        }

        $parsed['path'] = preg_replace(',spip\.php$,i', '', $parsed['path'] ?? '');
        $this->parsed = array_merge(['path' => '/'], $parsed);

        return $this;
    }

    public function glue(): string {
        return $this->parsed['scheme'] . '://' . $this->parsed['host'] . $this->parsed['path'];
    }

    public function getHost(): string
    {
        return $this->parsed['host'] ?? '';
    }

    public function getPath(): string
    {
        return $this->parsed['path'] ?? '';
    }

    public function getQuery(): string
    {
        return $this->parsed['query'] ?? '';
    }
}
