<?php

namespace App\Service;

use App\Entity\Site;

class Detector
{
    private Site $site;

    public function setSite(Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function checkComposedBy(): string
    {
        return $this->site->getHeader('composed-by');
    }

    public  function checkPhp(): string
    {
        return \preg_replace(',^php/,i', '', $this->site->getHeader('x-powered-by'));
    }

    public  function checkServer(): string
    {
        return $this->site->getHeader('server');
    }
}
