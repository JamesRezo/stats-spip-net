<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GiteaPaginator
{
    public function __construct(
        private HttpClientInterface $gitSpipNetClient,
        private int $limit = 50,
    ) {
    }

    public function withLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function findMaxPages(string $apiUrl, array $vars = []): int
    {
        $options = ['query' => ['limit' => $this->limit]];
        if (!empty($vars)) {
            $options['vars'] = $vars;
        }
        $response = $this->gitSpipNetClient->request('GET', $apiUrl, $options);
        $headers = [];
        foreach ($this->gitSpipNetClient->stream($response) as $chunk) {
            if ($chunk->isFirst()) {
                $headers = $response->getHeaders();
                $response->cancel();
            }
        }

        $pagination_values = [];
        if (isset($headers['access-control-expose-headers'])) {
            $pagination = $headers['access-control-expose-headers'][0];
            $pagination = \array_map('trim', \explode(',', $pagination));
            foreach ($pagination as $key) {
                $pagination_values[$key] = $headers[\strtolower($key)][0];
            }
        }

        $pageMax = 1;
        if (isset($pagination_values['Link'])) {
            foreach (explode(',', $pagination_values['Link']) as $value) {
                if (\preg_match(',^<([^>]+)>; rel="([^"]+)"$,', $value, $matches)) {
                    $rel = $matches[2];
                    $request = Request::create($matches[1]);
                    $page = $request->get('page', 1);
                    if ($rel == 'last') {
                        $pageMax = intval($page);
                    }
                }
            }
        }

        return $pageMax;
    }

    public function getPage(string $apiUrl, int $page = 1, array $vars = []): ResponseInterface
    {
        $options = ['query' => ['page' => $page, 'limit' => $this->limit]];
        if (!empty($vars)) {
            $options['vars'] = $vars;
        }

        return $this->gitSpipNetClient->request('GET', $apiUrl, $options);
    }

    public function getAll(array $responses): array
    {
        $pages = [];
        foreach ($this->gitSpipNetClient->stream($responses) as $response => $chunk) {
            if ($chunk->isLast())  {
                $pages = \array_merge($pages, $response->toArray());
            }
        }

        return $pages;
    }
}
